Name:		        krusader
Version:	      2.8.1
Release:	      1
Summary:	      An advanced twin-panel (commander-style) file-manager for KDE
License:	      GPL-2.0-or-later
URL:		        https://www.krusader.org/
Source0:	      https://download.kde.org/stable/%{name}/%{version}/%{name}-%{version}.tar.xz

BuildRequires:	bzip2-devel
BuildRequires:	cmake
BuildRequires:	desktop-file-utils
BuildRequires:	extra-cmake-modules
BuildRequires:	kf5-karchive-devel
BuildRequires:	kf5-kbookmarks-devel
BuildRequires:	kf5-kcodecs-devel
BuildRequires:	kf5-kcompletion-devel
BuildRequires:	kf5-kconfig-devel
BuildRequires:	kf5-kcoreaddons-devel
BuildRequires:	kf5-kdoctools-devel
BuildRequires:	kf5-kguiaddons-devel
BuildRequires:	kf5-ki18n-devel
BuildRequires:	kf5-kiconthemes-devel
BuildRequires:	kf5-kio-devel >= 5.23.0
BuildRequires:	kf5-kitemviews-devel
BuildRequires:	kf5-knotifications-devel
BuildRequires:	kf5-kparts-devel
BuildRequires:	kf5-ktextwidgets-devel
BuildRequires:	kf5-kwallet-devel
BuildRequires:	kf5-kwidgetsaddons-devel
BuildRequires:	kf5-kwindowsystem-devel
BuildRequires:	kf5-kxmlgui-devel
BuildRequires:	kf5-solid-devel
BuildRequires:	libacl-devel
BuildRequires:	libappstream-glib
BuildRequires:	libattr-devel
BuildRequires:	ninja-build
BuildRequires:	qt5-qtbase-devel
BuildRequires:	zlib-devel


%description
Krusader is an advanced twin panel (commander style) file manager for KDE and
other desktops in the *nix world, similar to Midnight or Total Commander.
It provides all the file management features you could possibly want.
Plus: extensive archive handling, mounted filesystem support, FTP, advanced
search module, an internal viewer/editor, directory synchronisation,
file content comparisons, powerful batch renaming and much much more.
It supports a wide variety of archive formats and can handle other KIO slaves
such as smb or fish. It is (almost) completely customizable, very user
friendly, fast and looks great on your desktop! You should give it a try.


%prep
%autosetup -p1


%build
%cmake_kf5 \
  -DCMAKE_BUILD_TYPE=RelWithDebInfo
%cmake_build


%install
%cmake_install
%find_lang %{name} --with-kde


%check
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/*.appdata.xml
desktop-file-validate %{buildroot}%{_datadir}/applications/*.desktop


%files -f %{name}.lang
%doc AUTHORS ChangeLog README README.md NEWS TODO
%license LICENSES/*
%{_bindir}/%{name}
%{_datadir}/%{name}/
%{_datadir}/applications/*.desktop
%{_datadir}/doc/HTML/*/%{name}/
%{_datadir}/icons/hicolor/*/apps/*.png
%{_datadir}/kxmlgui5/%{name}/
%{_libdir}/qt5/plugins/kf5/kio/kio*.so
%{_mandir}/*/man1/%{name}.1*
%{_mandir}/man1/%{name}.1*
%{_metainfodir}/*.appdata.xml
%{_sysconfdir}/xdg/kio_isorc


%changelog
* Wed Jan 01 2025 Funda Wang <fundawang@yeah.net> - 2.8.1-1
- update to 2.8.1

* Thu Nov 21 2024 tangjie02 <tangjie02@kylinsec.com.cn> - 2.8.0-2
- adapt to the new CMake macros to fix build failure

* Thu Jan 04 2024 misaka00251 <liuxin@iscas.ac.cn> - 2.8.0-1
- Init pacakge
